plugins {
    application
    kotlin("jvm") version "2.0.0"
}

group = "net.codebot"
version = "1.0"

repositories {
    mavenCentral()
}

dependencies {
}

kotlin {
    jvmToolchain(17)
}

application {
    mainClass.set("MainKt")
}