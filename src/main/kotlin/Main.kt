import java.io.File
import java.util.*

/* Collection of options
 * used instead of HashMap because we can add helper functions
 * NOTE: this was naively ported from Java; this is NOT idiomatic Kotlin!
 */

// command-line switches (where each is unique)
const val KEY_DIR: String = "dir"
const val KEY_DIRS_ONLY: String = "-d"
const val KEY_SHOW_HIDDEN: String = "-a"
const val KEY_MAX_DEPTH: String = "-l"
const val KEY_SHOW_COLOUR: String = "-c"
const val KEY_DISPLAY_HELP: String = "-h"
const val KEY_DISPLAY_HELP_ALT: String = "-help"

const val ANSI_RESET: String = "\u001B[0m"
const val ANSI_BLACK: String = "\u001B[30m"
const val ANSI_RED: String = "\u001B[31m"
const val ANSI_GREEN: String = "\u001B[32m"
const val ANSI_YELLOW: String = "\u001B[33m"
const val ANSI_BLUE: String = "\u001B[34m"
const val ANSI_PURPLE: String = "\u001B[35m"
const val ANSI_CYAN: String = "\u001B[36m"
const val ANSI_WHITE: String = "\u001B[37m"

object Options {
    private val lookup = HashMap<String, String>()
    fun put(key: String, value: String) {
        lookup[key] = value
    }

    fun get(key: String): String? = lookup[key]
    fun isEnabled(key: String): Boolean = lookup[key] == "true"
    fun contains(key: String): Boolean = lookup.containsKey(key)
}

// Helper functions
fun getSizeInKB(f: File): Double = (f.length() / 1024f).toDouble()
fun getSizeInMB(f: File): Double = getSizeInKB(f) / 1024
fun getSizeInGB(f: File): Double = getSizeInMB(f) / 1024

/*
 * Kotlin version of Unix Tree command
 * Display the contents of a directory in a hierarchy
 */
fun main(args: Array<String>) {
    Options.put(KEY_DIR, ".")
    Options.put(KEY_DIRS_ONLY, "false")
    Options.put(KEY_SHOW_HIDDEN, "false")
    Options.put(KEY_SHOW_COLOUR, "true")
    Options.put(KEY_MAX_DEPTH, "1")
    Options.put(KEY_DISPLAY_HELP, "false")
    Options.put(KEY_DISPLAY_HELP_ALT, "false")

    // options that we process before displaying the tree
    val validOptions = processArguments(args)
    if (Options.isEnabled(KEY_DISPLAY_HELP) || Options.isEnabled(KEY_DISPLAY_HELP_ALT) || !validOptions) {
        printHelp()
        System.exit(0)
    }

    // display results using options above
    walk(Options.get(KEY_DIR), 1)
}

// parse command-line arguments and store results in options lookup table
fun processArguments(args: Array<String>): Boolean {
    var key = ""
    var value = ""
    for (arg in args) {
        val m_arg = arg.lowercase(Locale.getDefault())
        // process arguments:
        //  key does not exist in set? invalid so exit
        //  new key && previous key not saved --> save old key/value, store new key, reset value
        //  new key && no previous key --> store new key, clear value
        if (m_arg.startsWith("-")) {
            if (!Options.contains(m_arg)) {
                println("Invalid key: $m_arg")
                return false
            }
            if (!key.isEmpty()) {
                Options.put(key, value)
            }
            key = m_arg
            value = "true"

            //  new value && previous key --> store new value
            //  new value && no previous key --> filename
        } else {
            if (!key.isEmpty()) {
                value = m_arg
            } else {
                Options.put(KEY_DIR, m_arg)
            }
        }
    }

    // write out last option
    if (!key.isEmpty()) {
        Options.put(key, value)
    }
    return true
}

// user help
fun printHelp() {
    println("Usage: tree [dir] [-option value]")
    println(" " + KEY_DIR + "  :: starting directory [" + Options.get(KEY_DIR) + "]")
    println(" " + KEY_DISPLAY_HELP + "   :: display this help and exit [" + Options.get(KEY_DISPLAY_HELP) + "]")
    println(" " + KEY_SHOW_COLOUR + "   :: show entries colorized [" + Options.get(KEY_SHOW_COLOUR) + "]")
    println(" " + KEY_DIRS_ONLY + "   :: list directories only [" + Options.get(KEY_DIRS_ONLY) + "]")
    println(" " + KEY_MAX_DEPTH + " n :: maximum display depth n [" + Options.get(KEY_MAX_DEPTH) + "]")
    println(" " + KEY_SHOW_HIDDEN + "   :: show hidden files [" + Options.get(KEY_SHOW_HIDDEN) + "]")
}

fun printColoredLine(colour: String, string: String) {
    println(colour + string + ANSI_RESET)
}

// display functions
fun getBranch(depth: Int): String {
    return if (depth == 1) {
        ""
    } else {
        " ".repeat(depth - 1) + "└─"
    }
}

// print search results
fun walk(path: String?, depth: Int) {
    // don't exceed depth
    if (depth > Options.get(KEY_MAX_DEPTH)!!.toInt()) {
        return
    }

    val root = File(path)
    val list = root.listFiles() ?: return

    // alphabetical list by default
    Arrays.sort(list)

    // traverse the tree
    for (f in list) {
        if ((f.isHidden && Options.isEnabled(KEY_SHOW_HIDDEN)) || !f.isHidden) {
            if (f.isDirectory) {
                if (Options.isEnabled(KEY_SHOW_COLOUR)) {
                    printColoredLine(getBranch(depth) + ANSI_CYAN, f.name)
                } else {
                    printColoredLine(getBranch(depth) + ANSI_BLACK, f.name)
                }
                walk(f.absolutePath, depth + 1)
            } else {
                if (!Options.isEnabled(KEY_DIRS_ONLY)) {
                    if (f.canExecute() && Options.isEnabled(KEY_SHOW_COLOUR)) {
                        printColoredLine(getBranch(depth) + ANSI_RED, f.name)
                    } else {
                        printColoredLine(getBranch(depth) + ANSI_BLACK, f.name)
                    }
                }
            }
        }
    }
}